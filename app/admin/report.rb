ActiveAdmin.register_page "Report" do


  content do
    results = ActiveRecord::Base.connection.execute("
      SELECT 
        statistics.name as name, 
        statistics.category as category, 
        SUM(statistics.count) as count, 
        SUM(statistics.result) as result 
      FROM (
        SELECT 
          products.name as name, 
          product_categories.name as category, 
          order_positions.count as count,
          order_positions.price*order_positions.count as result 
        FROM products 
        RIGHT OUTER JOIN product_categories 
          ON products.product_category_id = product_categories.id  
        RIGHT OUTER JOIN order_positions 
          ON order_positions.product_id = products.id
      ) as statistics 
      GROUP BY statistics.name, statistics.category").to_a
    
    render partial: 'report', locals: { results: results }
  end

end

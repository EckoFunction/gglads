class AddCountToOrpderPosition < ActiveRecord::Migration[5.0]
  def change
    add_column :order_positions, :count, :integer, null: false, default: 1
  end
end

module Api::V1
  class SessionsController < ApiController

    def create
      user = User.find_by(email: sessions_params[:email])
      if user
        if user.valid_password?(sessions_params[:password])
          json_response(SessionSerializer.new(user, root: false).to_h, 201)
        else
          json_response({}, 401)
        end
      else
        raise ActiveRecord::RecordNotFound, 'User not found'
      end
    end

    private

      def sessions_params
        params.require(:user).permit([:email, :password])
      end

  end
end

# == Schema Information
#
# Table name: products
#
#  id                  :integer          not null, primary key
#  name                :string
#  price               :decimal(8, 2)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  product_category_id :integer
#  deleted_at          :datetime
#

class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :price

  has_many :product_category, serializer: ProductCategorySerializer
end

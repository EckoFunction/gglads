# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170223182827) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "order_positions", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "order_id"
    t.decimal  "price",      precision: 8, scale: 2
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "count",                              default: 1, null: false
    t.index ["order_id"], name: "index_order_positions_on_order_id", using: :btree
    t.index ["price"], name: "index_order_positions_on_price", using: :btree
    t.index ["product_id"], name: "index_order_positions_on_product_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "shop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shop_id"], name: "index_orders_on_shop_id", using: :btree
  end

  create_table "product_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "product_category_id"
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.decimal  "price",               precision: 8, scale: 2
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "product_category_id"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_products_on_deleted_at", using: :btree
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                default: "",    null: false
    t.string   "encrypted_password",   default: "",    null: false
    t.datetime "remember_created_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "authentication_token"
    t.boolean  "admin",                default: false, null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

end

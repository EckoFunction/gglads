# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  email                :string           default(""), not null
#  encrypted_password   :string           default(""), not null
#  remember_created_at  :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  authentication_token :string
#  admin                :boolean
#

class User < ApplicationRecord
  devise :database_authenticatable, :rememberable, :validatable

  before_create :generate_authentication_token
  before_save :enshure_authenticate_token

  def enshure_authenticate_token
    generate_authentication_token unless authentication_token
  end

  def generate_authentication_token
    loop do
      self.authentication_token = SecureRandom.base64(64)
      break unless User.find_by(authentication_token: authentication_token)
    end
  end

  def admin?
    admin
  end

end

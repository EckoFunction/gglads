class ProductCategoryWithProductsSerializer < ProductCategorySerializer
  has_many :products
end

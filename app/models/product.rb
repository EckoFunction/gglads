# == Schema Information
#
# Table name: products
#
#  id                  :integer          not null, primary key
#  name                :string
#  price               :decimal(8, 2)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  product_category_id :integer
#  deleted_at          :datetime
#

class Product < ApplicationRecord
  acts_as_paranoid
  
  NAME_REGEX = /\A[а-яА-ЯёЁa-zA-Z]+[ а-яА-ЯёЁ\w]*\z/

  validates :name, presence: true, format: { with: NAME_REGEX }, length: { minimum: 2 }
  validates :price, presence: true, numericality: { geater_than: 0 }

  belongs_to :product_category
  has_many :order_positions
  has_many :orders, through: :order_positions
  
end

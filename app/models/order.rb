# == Schema Information
#
# Table name: orders
#
#  id         :integer          not null, primary key
#  shop_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Order < ApplicationRecord

  validates :shop, presence: true

  belongs_to :shop
  has_many :order_positions
  has_many :products, through: :order_positions

end

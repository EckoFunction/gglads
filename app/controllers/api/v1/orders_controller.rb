module Api::V1
  class OrdersController < ApiController
    before_action :check_user_logged_in

    def add_to_cart
      product = Product.find(params[:product_id].to_i)
      if product
        cart = session[:cart] || {}
        
        if cart[product.id.to_s].nil?
          (cart[product.id.to_s] = {"price" => product.price.to_s, "count" => cart_params[:count]})
        else
          cart[product.id.to_s]["count"] = cart[product.id.to_s]["count"].to_i + cart_params[:count].to_i
        end

        json_response(session[:cart] = cart)
      end
    end

    def remove_from_cart
      product = Product.find(params[:product_id].to_i)
      if product
        cart = session[:cart] || {}
        
        if cart[product.id.to_s].present?
          if cart[product.id.to_s]["count"] <= cart_params[:count].to_i
            cart = cart.dup.except(product.id.to_s)
          else
            cart[product.id.to_s]["count"] = cart[product.id.to_s]["count"].to_i - cart_params[:count].to_i
          end
        end

        json_response(session[:cart] = cart)
      end
    end

    def clear_cart
      session[:cart] = {}
      json_response(session[:cart])
    end

    def get_cart
      json_response(session[:cart] || {data: "Cart is empty"})
    end

    def checkout_cart
      cart = session[:cart] || {}

      if cart.present?
        order = Order.new

        cart.each do |product_id, data|
          product = Product.find_by(id: product_id.to_i)
          next unless product

          order.order_positions.build(product: product, price: data["price"], count: data["count"])
        end

        order.shop = Shop.first
        if order.save!
          session[:cart] = {}  
          json_response(order)
        end

      else
        json_response({ data: "Cart is empty" })
      end
    end

    private
    def cart_params
      params.require(:cart).permit(:count)
    end
  end
end

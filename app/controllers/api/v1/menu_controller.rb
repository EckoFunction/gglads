module Api::V1
  class MenuController < ApiController

    def index
      categories = ProductCategory.where(sub_categories: nil)
      json_response(categories, test: 'test')
    end

    def category_products
      category = ProductCategory.find(params[:category_id].to_i)
      json_response(ProductCategoryWithProductsSerializer.new(category).to_json)
    end

  end
end

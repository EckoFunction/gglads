# == Schema Information
#
# Table name: product_categories
#
#  id                  :integer          not null, primary key
#  name                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  product_category_id :integer
#

class ProductCategory < ApplicationRecord

  NAME_REGEX = /\A[а-яА-ЯёЁa-zA-Z]+[ а-яА-ЯёЁ\w]*\z/

  validates :name, presence: true, format: { with: NAME_REGEX }, length: { minimum: 2 }

  has_many :products
  has_many :sub_categories, class_name: ProductCategory, foreign_key: :product_category_id 
  belongs_to :parent_category, class_name: ProductCategory, foreign_key: :product_category_id

end

class CreateOrderPositions < ActiveRecord::Migration[5.0]
  def change
    create_table :order_positions do |t|
      t.references :product
      t.references :order
      t.decimal :price, precision: 8, scale: 2, index: true

      t.timestamps
    end
  end
end

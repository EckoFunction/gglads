class SessionSerializer < ActiveModel::Serializer
  attributes :email, :authentication_token
end

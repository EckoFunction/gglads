# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

for j in 0..9 do
  category = ProductCategory.find_or_create_by(name: "test category #{j}")

  for i in (j*10)..(j*10 + 9) do
    Product.find_or_create_by(name: "product_#{i}", price: rand(10.0...100.0), product_category: category )
  end
end

category = ProductCategory.first
category.sub_categories = ProductCategory.all.drop(1)
category.save

for i in 0..3 do
  Shop.create(name: "test shop name #{i}")
end

%w(admin manager).each do |role|
  u = User.find_by(email: "some@#{role}.email")
  User.create(email: "some@#{role}.email", 
              password: '123456789', 
              password_confirmation: '123456789', 
              admin: (role == 'admin') ? true : false) unless u
end
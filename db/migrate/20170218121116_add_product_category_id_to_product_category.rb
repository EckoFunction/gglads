class AddProductCategoryIdToProductCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :product_categories, :product_category_id, :integer
  end
end

module Api::V1
  class ApiController < ::ApiController
    include Response
    include ExceptionHandler

    def check_user_logged_in
      unless User.find_by(authentication_token: request.headers["X-Auth-Token"])
        raise ActiveRecord::RecordNotFound, 'User not found'
      end
    end

  end
end

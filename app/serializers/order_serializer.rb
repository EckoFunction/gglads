# == Schema Information
#
# Table name: orders
#
#  id         :integer          not null, primary key
#  shop_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class OrderSerializer < ActiveModel::Serializer
  attributes :id, :products
end

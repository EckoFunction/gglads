# == Schema Information
#
# Table name: order_positions
#
#  id         :integer          not null, primary key
#  product_id :integer
#  order_id   :integer
#  price      :decimal(8, 2)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  count      :integer          default(1), not null
#

class OrderPosition < ApplicationRecord

  belongs_to :order
  belongs_to :product

  validates :order, presence: true
  validates :product, presence: true
  validates :count, presence: true, numericality: { greater_than_or_equal_to: 1 }

  before_save do
    self.price = product.price if price.blank?
  end

end

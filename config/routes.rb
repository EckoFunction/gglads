Rails.application.routes.draw do

  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace 'api' do
    namespace :v1 do
      post 'sign_in', to: 'sessions#create', as: :sign_in

      get 'menu', to: 'menu#index'
      get 'menu/categories/:category_id', to: 'menu#category_products', as: :menu_category

      post '/add_to_cart/:product_id', to: 'orders#add_to_cart', as: :add_to_cart
      post '/remove_from_cart/:product_id', to: 'orders#remove_from_cart', as: :remove_from_cart
      post '/clear_cart', to: 'orders#clear_cart', as: :clear_cart

      get '/get_cart', to: 'orders#get_cart', as: :get_cart
      post '/checkout_cart', to: 'orders#checkout_cart', as: :checkout_cart

    end
  end

end

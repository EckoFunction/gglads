# == Schema Information
#
# Table name: product_categories
#
#  id                  :integer          not null, primary key
#  name                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  product_category_id :integer
#

class ProductCategorySerializer < ActiveModel::Serializer
  attributes :id, :name

  attribute :sub_categories, if: -> { object.sub_categories.present? }
  attribute :parent_category, if: -> { object.parent_category.present? }

  link(:self) { api_v1_menu_category_path(object) }

end
